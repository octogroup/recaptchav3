export declare const makeReCAPTCHAEndpoint: (reCAPTCHAv3Key: string) => string;
