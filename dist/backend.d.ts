import { reCAPTCHAAction } from './index';
export interface reCAPTCHAVerifyResponse {
    success: boolean;
    score: number;
    action: string | reCAPTCHAAction;
    challenge_ts: string;
    hostname: string;
    'error-codes'?: string[];
}
