"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeReCAPTCHAEndpoint = void 0;
const _reCAPTCHAendpoint = 'https://www.google.com/recaptcha/api.js?render=';
exports.makeReCAPTCHAEndpoint = (reCAPTCHAv3Key) => _reCAPTCHAendpoint + reCAPTCHAv3Key;
//# sourceMappingURL=frontend.js.map