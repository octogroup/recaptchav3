import { reCAPTCHAAction } from './index';
export interface OCTOVerifyResponse {
    human: boolean | undefined;
    error: boolean;
}
export interface reCAPTCHACallbacks {
    onInteraction: (token: string) => void;
    onError: () => void;
}
export declare type InvokeReCAPTCHAOptions = reCAPTCHAAction & {
    sitekey: string;
    reCAPTCHAcbs?: reCAPTCHACallbacks;
};
export declare const reCAPTCHExecute: (options: InvokeReCAPTCHAOptions) => void;
