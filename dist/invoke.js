"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reCAPTCHExecute = void 0;
exports.reCAPTCHExecute = (options) => {
    if (options.reCAPTCHAcbs) {
        return grecaptcha.ready(() => {
            if (options.reCAPTCHAcbs) {
                grecaptcha.execute(options.sitekey, options.action).then(options.reCAPTCHAcbs.onInteraction, options.reCAPTCHAcbs.onError);
            }
            else {
                throw new Error('reCAPTCHA Callbacks are undefined in an impossible situation.');
            }
        });
    }
    else {
        return grecaptcha.ready(() => {
            grecaptcha.execute(options.sitekey, options.action);
        });
    }
};
//# sourceMappingURL=invoke.js.map