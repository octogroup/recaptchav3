import { OCTOVerifyResponse, reCAPTCHACallbacks, InvokeReCAPTCHAOptions, reCAPTCHExecute } from './invoke';
export { OCTOVerifyResponse, reCAPTCHACallbacks, InvokeReCAPTCHAOptions, reCAPTCHExecute };
import { makeReCAPTCHAEndpoint } from './frontend';
export { makeReCAPTCHAEndpoint };
import { reCAPTCHAVerifyResponse } from './backend';
export { reCAPTCHAVerifyResponse };
export interface reCAPTCHAHomepage {
    action: 'homepage';
}
export interface reCAPTCHAPage {
    action: 'page';
}
export interface reCAPTCHARegister {
    action: 'register';
}
export interface reCAPTCHALogin {
    action: 'login';
}
export interface reCAPTCHAResetPassword {
    action: 'reset';
}
export interface reCAPTCHAForm {
    action: 'form';
}
export interface reCAPTCHAFormSubmit {
    action: 'submit';
}
export interface reCAPTCHAInteractive {
    action: 'interactive';
}
export declare type reCAPTCHAAction = reCAPTCHAHomepage | reCAPTCHAPage | reCAPTCHARegister | reCAPTCHALogin | reCAPTCHAResetPassword | reCAPTCHAForm | reCAPTCHAInteractive | reCAPTCHAFormSubmit;
export declare type reCAPTCHAActionStrings = 'page' | 'homepage' | 'register' | 'login' | 'reset' | 'form' | 'submit' | 'interactive';
