"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeReCAPTCHAEndpoint = exports.reCAPTCHExecute = void 0;
const invoke_1 = require("./invoke");
Object.defineProperty(exports, "reCAPTCHExecute", { enumerable: true, get: function () { return invoke_1.reCAPTCHExecute; } });
const frontend_1 = require("./frontend");
Object.defineProperty(exports, "makeReCAPTCHAEndpoint", { enumerable: true, get: function () { return frontend_1.makeReCAPTCHAEndpoint; } });
//# sourceMappingURL=index.js.map