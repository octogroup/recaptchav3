import { reCAPTCHAAction } from './index';

/**
 * The Head of the front-end needs to be patched with:
 *
 * <script src="https://www.google.com/recaptcha/api.js?render=<reCAPTCHAv3Key>"/>
 */

// Script src references the API key.
const _reCAPTCHAendpoint = 'https://www.google.com/recaptcha/api.js?render=';
export const makeReCAPTCHAEndpoint = (reCAPTCHAv3Key: string) => _reCAPTCHAendpoint + reCAPTCHAv3Key;

/**
 * We can hide the badge in our SCSS with: .grecaptcha-badge { visibility: hidden; }
 * 
 * But we need to make sure we state something like the following after every form:
 *   "This site is protected by reCAPTCHA and the Google
 *     <a href="https://policies.google.com/privacy">Privacy Policy</a> and
 *     <a href="https://policies.google.com/terms">Terms of Service</a> apply."
 */