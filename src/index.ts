import { OCTOVerifyResponse, reCAPTCHACallbacks, InvokeReCAPTCHAOptions, reCAPTCHExecute } from './invoke';
export { OCTOVerifyResponse, reCAPTCHACallbacks, InvokeReCAPTCHAOptions, reCAPTCHExecute };

import { makeReCAPTCHAEndpoint } from './frontend';
export { makeReCAPTCHAEndpoint };

import { reCAPTCHAVerifyResponse } from './backend';
export { reCAPTCHAVerifyResponse };

/**
 * V3 Keys for MarXiv
 * Site Key: 6Le5MbQUAAAAAHgMWbTWhx7FSrmsgv43YllnqFrL
 * Secret Key: 6Le5MbQUAAAAANK6i5Xd5u-HTG1MKot2ybUlvED2
 */

/**
 * You get a "detailed break-down" of your top 10 actions in the admin console
 * We'll use 8 static actions, so as not to exceed the soft-limit of 10.
 */
export interface reCAPTCHAHomepage {
   action: 'homepage'
}
export interface reCAPTCHAPage {
   action: 'page'
}
export interface reCAPTCHARegister {
   action: 'register'
}
export interface reCAPTCHALogin {
   action: 'login'
}
export interface reCAPTCHAResetPassword {
   action: 'reset'
}
/**
 * General "low-key" forms, like surveys.
 */
export interface reCAPTCHAForm {
   action: 'form'
}
/**
 * More important forms, like the MarXiv submission form.
 */
export interface reCAPTCHAFormSubmit {
   action: 'submit'
}
/**
 * Any kind of interactive component, e.g. link, button, etc.
 */
export interface reCAPTCHAInteractive {
   action: 'interactive'
}
/**
 * Union type for our static actions.
 * @param action May only contain alphanumeric characters and slashes, and must not be user-specific.
 */
export type reCAPTCHAAction = reCAPTCHAHomepage | reCAPTCHAPage | reCAPTCHARegister | reCAPTCHALogin | reCAPTCHAResetPassword | reCAPTCHAForm | reCAPTCHAInteractive | reCAPTCHAFormSubmit;
export type reCAPTCHAActionStrings = 'page' | 'homepage' | 'register' | 'login' | 'reset' | 'form' | 'submit' | 'interactive';