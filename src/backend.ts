import { reCAPTCHAAction } from './index';

/**
 * The back-end will get this response.
 * We want to return to the front-end an OCTOVerifyResponse.
 * 
 * @param score Scale from very likely a bot (0) to very likely a human (1)
 * @param challenge_ts Timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
 * @param hostname Site where the reCAPTCHA was solved
 */
export interface reCAPTCHAVerifyResponse {
	success: boolean
	score: number
	action: string | reCAPTCHAAction
	challenge_ts: string
	hostname: string
	'error-codes'?: string[]
}