import { reCAPTCHAAction, reCAPTCHAActionStrings } from './index'

/**
 * We'll have the back-end send us back a better response.
 * @param human True for a human, false for a bot, otherwise undefined if any error. Depending on the Action, the threshhold for 'human' may be higher than others.
 * @param error Boolean to invoke the error callback (i.e. "Try again later" because we can't contact Google).
 */
export interface OCTOVerifyResponse {
	human: boolean | undefined
	error: boolean
}

/**
 * Callback functions to operate onSubmit, onError, and onRetry
 * 
 * @param onInteraction Callback to run onClick or onSubmit or whatever, with verification if needed.
 * @param onError Only use in conjunction with verification.
 */
export interface reCAPTCHACallbacks {
	onInteraction: (token: string) => void
	onError: () => void
}

/**
 * Let's not work with promises directly!
 * @param sitekey reCAPTCHA v3 site key. Register one at https://g.co/recaptcha/v3
 * @param action 1 of 8 standardized reporting actions, which are simple strings.
 * @param reCAPTCHACallbacks If the user's interaction needs to be verified, include this in the onInteraction callback, which must handle humans and bots.
 */
export type InvokeReCAPTCHAOptions = reCAPTCHAAction & {
	sitekey: string
	reCAPTCHAcbs?: reCAPTCHACallbacks
}

/**
 * @param reCAPTCHAv3Key reCAPTCHA v3 site key. Register one at https://g.co/recaptcha/v3
 */
interface reCAPTCHA {
	ready(callback: () => void): void
	execute(reCAPTCHAv3Key: string, options: reCAPTCHAActionStrings): Promise<string>
}
// Establish grecaptcha.
declare const grecaptcha: reCAPTCHA;

export const reCAPTCHExecute = (options: InvokeReCAPTCHAOptions): void => {
	if (options.reCAPTCHAcbs) {
		// This reCAPTCHA requires verification.
		return grecaptcha.ready(
			() => {
				// TypeScript compiler forces us to re-check.
				if (options.reCAPTCHAcbs) {
					grecaptcha.execute(options.sitekey, options.action).then(options.reCAPTCHAcbs.onInteraction, options.reCAPTCHAcbs.onError)
				}
				else {
					throw new Error('reCAPTCHA Callbacks are undefined in an impossible situation.')
				}
			}
		)
	}
	else {
		return grecaptcha.ready(
			() => {
				grecaptcha.execute(options.sitekey, options.action);
			}
		)
	}
}
